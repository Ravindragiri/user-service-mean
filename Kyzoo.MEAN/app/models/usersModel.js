﻿import { Schema, model } from 'mongoose';

var users = new Schema({

    email: String,
    userName: String,
    password: String,
	customerId: String,
    clientId: String,
    securityQuestionId: String,
    SecurityAnswer: String,
    lastChangeDate: Date,
    isLocked: boolean,
    changePwdatLogin: boolean,
    effectiveDate: Date,
    expirationDate: Date
});

export const userSchema = users;
model('user', users);