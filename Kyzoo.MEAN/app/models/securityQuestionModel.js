﻿var mongoose = require('mongoose');

var securityQuestionSchema = new mongoose.Schema({

    securityQuestionText: string,
    active: number
});

exports.securityQuestionSchema = securityQuestionSchema;
mongoose.model('securityQuestion', securityQuestionSchema);