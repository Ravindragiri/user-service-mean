module.exports = function (app, db) {

    var userApiController = require('../controllers/api/user.server.api.controller');
    //get
    app.get('/api/user/checkCustomerExists', userApiController.checkCustomerExists);//
    app.get('/api/user/getUserSecurityQuestion', userApiController.getUserSecurityQuestion);
    app.get('/api/user/findUserId', userApiController.findUserId);//
    app.get('/api/user/getUserNameEmail', userApiController.getUserNameEmail);//
    app.get('/api/user/getUserDetails', userApiController.getUserDetails);//
    app.get('/api/user/getAllSecurityQuestions', userApiController.getAllSecurityQuestions);//
    

    //post
    app.post('/api/user/updateCustomerId', userApiController.updateCustomerId);//
    app.post('/api/user/authenticateUser', userApiController.authenticateUser);
    app.post('/api/user/checkUserSecurityAnswer', userApiController.checkUserSecurityAnswer);//
    app.post('/api/user/setUserSecurityQuestion', userApiController.setUserSecurityQuestion);//
    app.post('/api/user/updateUser', userApiController.updateUser);
    app.post('/api/user/checkPassword', userApiController.checkPassword);//
    app.post('/api/user/createNewUser', userApiController.createNewUser);//
    app.post('/api/user/updateEmail', userApiController.updateEmail);//
    app.post('/api/user/setPassword', userApiController.setPassword);//
    app.post('/api/user/resetPassword', userApiController.resetPassword);//
};