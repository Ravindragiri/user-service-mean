var utils = require('../common/utils'),
    ObjectId = require('mongodb').ObjectID;

require('../common/appConstants');

exports.updateCustId = async function (user) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({ $or: [{ _id: ObjectId(user._id) }, { userName: user.userName }] },
            {
                $set: {
                    "customerId": user.customerId, "effectiveDate": new Date(),
                    "lastChangeDate": new Date()
                }
            }));
}

exports.checkCustExists = async function (user) {
    return await getColl('user').then(c => c
        .findOne({ $or: [{ customerId: user.customerId }, { userName: user.userName }] }));
};

exports.createUser = async function (user) {
    user.password = utils.createHash(user.userName, user.password);
    user.effectiveDate = new Date();
    user.isLocked = false;
    user.lastChangeDate = new Date();
    user.changePwdatLogin = false;
    return await getColl('user').then(c => c.insertOne(user));
};

exports.getUserExistsByUserName = async function (userName) {
    return await getColl('user').then(c => c.findOne({ 'userName': userName }));
}

exports.getUserByUserNamePassword = async function (userName, password) {
    return await getColl('user').then(c => c.findOne({ $and: [{ userName: userName }, { password: password }] }));
}

exports.lockUserAccount = async function (userId) {

    return await getColl('user').then(c => c.findOneAndUpdate({ _id: ObjectId(userId) },
        {
            $set: {
                isLocked: true, effectiveDate: new Date(),
                lastChangeDate: new Date()
            }
        }));
}

exports.updateLoginAttempts = async function (userId, attempt, lastAttemptDate) {
    return await getColl('user').then(c => c.findOneAndUpdate({ _id: ObjectId(userId) },
        {
            $set: {
                attempt: attempt, lastAttemptDate: lastAttemptDate
            }
        }));
}

exports.insertLoginAttempts = async function (userId, attempt, lastAttemptDate) {
    return await getColl('user').then(c => c.findOneAndUpdate({ _id: ObjectId(userId) },
        {
            $set: {
                attempt: attempt, lastAttemptDate: lastAttemptDate
            }
        }));
}

exports.UpdUserResetLoginAttempts = async function (user) {
    var result = await updateLoginAttempts(ObjectId(user._id));
    if (result) {
        return await getColl('user').then(c => c
            .findOneAndUpdate({
                userName: user.userName, effectiveDate: new Date(),
                lastChangeDate: new Date()
            }));
    }
}

exports.submitUserSecurityAns = async function (user) {
    return await getColl('user').then(c => c
        .findOne(
            {
                $and: [
                    { securityAnswer: user.securityAnswer },
                    { securityQuestionId: ObjectId(user.securityQuestionId) },
                    {
                        $or: [
                            { customerId: user.customerId },
                            { _id: ObjectId(user._id) },
                            { userName: user.userName }
                        ]
                    }
                ]
            }));
}

exports.getUserSecurityQue = async function (user) {
    var result = await getColl('user').then(c => c.findOne({
        $and: [
            { active: true },
            {
                $or: [
                    { customerId: user.customerId },
                    { _id: ObjectId(user._id) },
                    { userName: user.userName }
                ]
            }
        ]
    }));
    if (result) {
        return { securityQuestionId: result.securityQuestionId, securityQuestionText: result.securityQuestionText };
    }
}

exports.isAccountLocked = async function (userId, lastThreeHours) {
    return await getColl('user').then(c => c
        .findOne(
            {
                $and: [
                    { _id: ObjectId(userId) },
                    { isLocked: true },
                    { "date": { "$gte": lastThreeHours } }
                ]
            }));
}

exports.updateUserName = async function (userId, userName) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({ _id: ObjectId(userId) },
            {
                $set: {
                    "userName": userName, effectiveDate: new Date(),
                    lastChangeDate: new Date()
                }
        }));
}

exports.updateClientId = async function (userId, clientId) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({ _id: ObjectId(userId) },
            {
                $set: {
                    clientId: clientId,
                    effectiveDate: new Date(),
                    lastChangeDate: new Date()
                }
            }));
}

exports.updatePassword = async function (user) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({ _id: ObjectId(user._id) },
            {
                $set: {
                    "password": utils.createHash(user.userName, user.password), effectiveDate: new Date(),
                    lastChangeDate: new Date()
                }
            }));
}

exports.updUserUnSetPwdAtLogin = async function (user) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({ userName: user.userName },
            {
                $set: {
                    "changePwdatLogin": false, effectiveDate: new Date(),
                    lastChangeDate: new Date()
                }
            }));
}

exports.updUserSetPwdAtLogin = async function (user) {

    return await getColl('user').then(c => c
        .findOneAndUpdate({ userName: user.userName },
            {
                $set: {
                    "changePwdatLogin": true, effectiveDate: new Date(),
                    lastChangeDate: new Date()
                }
            }));
}

exports.updateSecurityQuestionAnswer = async function (userId, securityQuestionId, securityAnswer) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({ _id: ObjectId(userId) },
            {
                $set: {
                    effectiveDate: new Date(),
                    lastChangeDate: new Date(),
                    securityQuestionId: ObjectId(securityQuestionId),
                    securityAnswer: securityAnswer
                }
            }));
}

exports.getUserId = async function (user) {
    return await getColl('user').then(c => c.findOne({
        $or: [
            { customerId: user.customerId },
            { userName: user.userName },
            { email: user.email }
        ]
    }));
}

exports.retrieveUserNameEmail = async function (user) {
    return await getColl('user').then(c => c.findOne({
        $or: [
            { customerId: user.customerId },
            { userName: user.userName }
        ]
    }));
}

exports.checkUserPassword = async function (user) {
    return await getColl('user').then(c => c.findOne({
        $and: [
            { password: utils.createHash(user.userName, user.password) },
            {
                $or: [
                    { customerId: user.customerId },
                    { userName: user.userName }
                ]
            }
        ]
    }));
}

exports.retrieveUserDetails = async function (user) {
    return await getColl('user').then(c => c.findOne({
        $or: [
            { customerId: user.customerId },
            { userName: user.userName },
            { email: user.email }
        ]
    }));
}

exports.retrieveAllSecurityQuestions = async function () {
    return await getColl('securityQuestion').then(c => c.find({}).toArray());
};

exports.setPassword = async function (user) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({
            $or: [
                { customerId: user.customerId },
                { userName: user.userName }
            ]
        }, {
                $set: {
                    password: user.password, effectiveDate: new Date(),
                    lastChangeDate: new Date(),
                    changePwdatLogin: false
                }
            }));
}

exports.changeEmail = async function (user) {
    return await getColl('user').then(c => c
        .findOneAndUpdate({
            $or: [
                { customerId: user.customerId },
                { _id: ObjectId(user._id) },
                { userName: user.userName }
            ]
        }, {
                $set: {
                    "email": user.email, effectiveDate: new Date(),
                    lastChangeDate: new Date()
                }
        }));
}