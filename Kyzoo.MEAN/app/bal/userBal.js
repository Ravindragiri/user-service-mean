var moment = require('moment'),
    utils = require('../common/utils');
var userDal = require('../../app/dal/userDal.js');
require('../common/helpers/responseHelper');
require('../common/emailUtility');

exports.updateCustId = async function (user) {
    return await userDal.updateCustId(user);
};

exports.checkCustExists = async function (user) {
    return await userDal.checkCustExists(user);
};

exports.createUser = async function (user) {
    return await userDal.createUser(user);
};

exports.authUser = async function (userName, password) {
    password = utils.createHash(userName, password);
    var result;
    var user = await userDal.getUserExistsByUserName(userName);
    if (!user) {
        result = { statusCode: 1 };
    }
    else {
        if (user.isAdminLocked || user.isLocked) {
            result = { statusCode: 3 };
        }
        var user2 = await userDal.getUserByUserNamePassword(userName, password);

        if (user2) {
            if (user2.changePwdatLogin) {
                result = { statusCode: 5, _id: user._id };
            }
            else {
                await userDal.updateLoginAttempts(user._id, 0);
                result = { statusCode: 0, _id: user._id };
            }
        }
        else {
            var pwdExpInterval = 15;
            var lastAttemptDate = new Date();
            if (user.attempt) {
                var attempt = user.attempt + 1;
                var diffInMins = moment.utc(moment(lastAttemptDate, "HH:mm:ss").diff(moment(user.lastAttemptDate, "HH:mm:ss"))).format("mm");
                if (diffInMins <= pwdExpInterval) {
                    if (attempt >= 4) {
                        userDal.lockUserAccount(user._id);
                        result = { statusCode: 3 };
                    }
                    else {
                        userDal.updateLoginAttempts(user._id, attempt, lastAttemptDate);
                        result = { statusCode: 2 };
                    }
                }
                else {
                    userDal.updateLoginAttempts(user._id, attempt, lastAttemptDate);
                    result = { statusCode: 2 };
                }
            }
            else {
                userDal.insertLoginAttempts(user._id, 1, lastAttemptDate);
                result = { statusCode: 2 };
            }
        }
    }
    return result;
};

exports.submitUserSecurityAns = async function (user) {
    return await userDal.submitUserSecurityAns(user);
};

exports.getUserSecurityQue = async function (user) {
    return await userDal.getUserSecurityQue(user);
};

exports.setUserSecurityQue = async function (user) {
    var userId;
    var resultUsers = await userDal.getUserId(user);
    if (resultUsers) {
        userId = resultUsers._id;
    }
    if (!userId) {
        user.StatusCode = -1;
        user.StatusText = "Unknown username/clientID";
    }
    else {
        var pwdExpInterval = 3;
        var myDate = new Date();
        var lastThreeHours = moment(myDate).subtract(pwdExpInterval, 'hours').toDate();
        var bUserIdExists = await userDal.isAccountLocked(userId, lastThreeHours);
        if (bUserIdExists) {
            user.statusCode = -1;
            user.statusText = "User is currently locked. Cannot update user account";
        }
        if (user.securityQuestionId) {
            user = await userDal.updateSecurityQuestionAnswer(userId, user.securityQuestionId, user.securityAnswer);
        }
    }
    return user;
};

exports.updateUserInfo = async function (user) {
    var result;
    var resultUsers = await userDal.getUserId(user);
    if (!resultUsers) {
        return {
            statusCode: -1,
            statusText: "Unknown username/clientId"
        };
    } else {
        var userId = resultUsers._id;
        var pwdExpInterval = 3;
        var myDate = new Date();
        var lastThreeHours = moment(myDate).subtract(pwdExpInterval, 'hours').toDate();
        var bUserIdExists = await userDal.isAccountLocked(userId, lastThreeHours);
        if (bUserIdExists) {
            return {
                statusCode: -1,
                statusText: "User is currently locked. Cannot update user account"
            };
        }
        else {
            if (!user.userName) {
                updateUserName(userId, userName);
            }
            if (!user.clientId) {
                updateClientId(userId, clientId);
            }
            if (!user.password) {
                updatePassword(userId, user.userName, user.password);
            }
            if (!user.securityQuestionId) {
                updateSecuritySecurityQuestion(userId, user.securityQuestionId, user.securityAnswer);
            }
        }
    }
    return result;
};

formatUpdateResponse = function (result) {
    var updateResult;
    var isUpdated = result.lastErrorObject.updatedExisting;
    if (isUpdated) {
        updateResult = { statusCode: statusCode.success.value, data: doc };
    } else {
        updateResult = { statusCode: statusCode.failed.value };
    }
    return updateResult;
};

exports.getUserId = async function (user) {
    return await userDal.getUserId(user);
};

exports.retrieveUserNameEmail = async function (user) {
    return await userDal.retrieveUserNameEmail(user);
};

exports.checkUserPassword = async function (user) {
    return await userDal.checkUserPassword(user);
};

exports.retrieveUserDetails = async function (user) {
    return await userDal.retrieveUserDetails(user);
};

exports.setPassword = async function (user) {
    var result = await userDal.updatePassword(user);
    if (result) {
        return await userDal.updUserUnSetPwdAtLogin(user);
    }
};

exports.resetPassword = async function (user) {
    var result = await userDal.updatePassword(user);
    if (result) {
        await userDal.updUserSetPwdAtLogin(user);
        await userDal.updUserResetLoginAttempts(user);
    }
};

exports.changeEmail = async function (user) {
    return await userDal.changeEmail(user);
};

exports.retrieveAllSecurityQuestions = async function () {
    return await userDal.retrieveAllSecurityQuestions();
};

