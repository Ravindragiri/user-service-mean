var userBal = require('../../../app/bal/userBal.js');
require('../../common/helpers/responseHelper');
require('../../common/emailUtility');

exports.updateCustomerId = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user.customerId && (user._id || user.userName)) {
        var result = await userBal.updateCustId(user);
        if (result) {
            sendMail();
            sendSuccessResponse(res, result, messages.UpdateSuccess);
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.checkCustomerExists = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user.customerId && user.userName) {
        var result = await userBal.checkCustExists(user);
        sendSuccessResponse(res, result);
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.authenticateUser = async function (req, res) {

    if (req.body.userName && req.body.password) {
        var userName = req.body.userName;
        var password = req.body.password;
        var result = await userBal.authUser(userName, password);
        res.send({ data: result });
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.checkUserSecurityAnswer = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && (user.customerId || user._id || user.userName)) {
        var result = await userBal.submitUserSecurityAns(user);
        if (result) {
            sendSuccessResponse(res, result);
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getUserSecurityQuestion = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.query));
    if (user && (user.customerId || user._id || user.userName)) {
        return await userBal.getUserSecurityQue(user);
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.setUserSecurityQuestion = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user.customerId && user.userName) {
        var result = await userBal.setUserSecurityQue(user);
        if (result) {
            sendMail();
            if (result.statusCiode === -1) {
                sendFailResponse(res, user, "User is currently locked. Cannot update user account");
            }
            else {
                sendSuccessResponse(res, result, messages.UpdateSuccess);
            }
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.updateUser = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user.customerId && user.userName) {
        var result = await userBal.updateUserInfo(user);
        if (result) {
            sendMail();
            sendSuccessResponse(res, result, messages.UpdateSuccess);
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.findUserId = async function (req, res) {
    if (req.query.email || req.query.customerId || req.query.userName) {
        var user = { email: req.query.email, customerId: req.query.customerId, userName: req.query.userName };
        var result = await userBal.getUserId(user);
        if (result) {
            sendSuccessResponse(res, result);
        }
        else {
            sendNoContentResponse(res);
        }

    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getUserNameEmail = async function (req, res) {

    if (req.query.customerId || req.query._id) {
        var user = { customerId: req.query.customerId, userId: req.query._id };
        var result = await userBal.retrieveUserNameEmail(user);
        if (result) {
            sendMail();
            sendSuccessResponse(res, { userName: result.userName, email: result.email });
        } else {
            sendNoContentResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.checkPassword = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user.password && (user.customerId || user.userName)) {
        var result = await userBal.checkUserPassword(user);
        if (result) {
            sendSuccessResponse(res);
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.createNewUser = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user.userName && user.password) {
        var userId = await userBal.getUserId(user);
        if (userId) {
            sendFailResponse(res, user, "Duplicate user");
        }
        else {
            var result = await userBal.createUser(user);
            if (result) {
                sendMail();
                sendSuccessResponse(res, result);
            } else {
                sendFailResponse(res);
            }
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getUserDetails = async function (req, res) {
    if (req.query.customerId || req.query._id || req.query.userName || req.query.clientId) {
        var user = {
            customerId: req.query.customerId, _id: req.query._id,
            userName: req.query.userName, clientId: req.query.clientId
        };
        var result = await userBal.retrieveUserDetails(user);
        if (result) {
            sendSuccessResponse(res, result);
        }
        else {
            sendNoContentResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.updateEmail = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user.email && (user.customerId || user._id || user.userName)) {
        var result = await userBal.changeEmail(user);
        if (result) {
            if (result.statusCode === statusCode.success.value) { sendMail(); }
            res.send({ data: result });
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.setPassword = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user._id && user.userName && user.password) {
        var result = await userBal.setPassword(user);
        if (result) {
            sendMail();
            sendSuccessResponse(res, "", messages.UpdateSuccess);
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.resetPassword = async function (req, res) {
    var user = JSON.parse(JSON.stringify(req.body));
    if (user && user._id && user.userName && user.password) {
        var result = await userBal.resetPassword(user);
        if (result) {
            sendMail();
            sendSuccessResponse(res, "", messages.UpdateSuccess);
        } else {
            sendFailResponse(res);
        }
    }
    else {
        sendBadRequestResponse(res);
    }
};

exports.getAllSecurityQuestions = async function (req, res) {
    var result = await userBal.retrieveAllSecurityQuestions();
    sendSuccessResponse(res, result);
};
