﻿require('../../common/appConstants');

sendSuccessResponse = function (res, result, message) {
    res.send({
        status: statusCode.success.text,
        data: result,
        message: message,
        statusCode: statusCode.success.value
    });
};

sendFailResponse = function (res, result, message) {
    res.send({
        status: statusCode.failed.text,
        data: result,
        message: message,
        statusCode: statusCode.failed.value
    });
};

sendValidationFailedResponse = function (res) {
    res.render('home.html', {
        errors: errors,
        statusCode: statusCode.serverValidationFailed.value
    });
};

sendErrorResponse = function (res) {
    res.send({
        status: statusCode.error.text,
        message: messages.errorProcessing,
        statusCode: statusCode.error.value
    });
};

sendBadRequestResponse = function (res, data) {
    res.status(400).send({
        status: statusCode.error.text,
        data: data,
        message: messages.badRequest,
        statusCode: statusCode.error.value
    });
};

sendUnauthorizedResponse = function (res, data) {
    res.status(401).send({
        status: statusCode.error.text,
        data: data,
        message: messages.unauthorizedAccess,
        statusCode: statusCode.error.value
    });
};

sendNoContentResponse = function (res) {
    res.status(204).send({
        status: statusCode.notFound.text,
        data: "",
        message: messages.notFound,
        statusCode: statusCode.notFound.value
    });
};

exports.sendSuccessResponse = sendSuccessResponse;
exports.sendFailResponse = sendFailResponse;
exports.sendValidationFailedResponse = sendValidationFailedResponse;
exports.sendErrorResponse = sendErrorResponse;
exports.sendBadRequestResponse = sendBadRequestResponse;
exports.sendUnauthorizedResponse = sendUnauthorizedResponse;
exports.sendNoContentResponse = sendNoContentResponse;