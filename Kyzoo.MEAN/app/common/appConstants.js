﻿statusCode = {

    SYSTEM_ERROR: { text: 'SYSTEM_ERROR', value: 500 },
    serverValidationFailed: { text: 'ServerValidationFailed', value: -10 },
    notFound: { text: 'NotFound', value: 204 },
    fkConflict: { text: 'FKConflict', value: -8 },
    unauthorizedAccess: { text: 'UnauthorizedAccess', value: 401 },
    deleteFailed: { text: 'DeleteFailed', value: -6 },
    alreadyExists: { text: 'AlreadyExists', value: -5 },
    invalid: { text: 'Invalid', value: -4 },
    failedRedirect: { text: 'FailedRedirect', value: -3 },
    errorRedirect: { text: 'ErrorRedirect', value: -2 },
    error: { text: 'Error', value: -1 },
    failed: { text: 'Failed', value: 0 },
    success: { text: 'Success', value: 200 },
    redirect: { text: 'Redirect', value: 300 }
};

messages = {
    saveSuccess: "Record has been saved successfully.",
    updateSuccess: "Record has been updated successfully.",
    deleteSuccess: "Record has been deleted successfully.",
    readSuccess: "Record has been retrieved successfully.",

    saveFailed: "We're unable to save your record.",
    deleteFailed: "We're unable to delete your record.",

    errorProcessing: "The system encountered an error while processing your request.",
    errorProcessingRefNumber: "The system encountered an error while processing your request. Reference number: {0}",

    userAlreadyExists: "The user is already registered!",
    userRegisteredSuccessfully: "The user is registered successfully!",
    userRegistrationFailed: "Registration failed.",
    userDoesnotExist: "User does not exist!",
    usernameNotAvailable: "User already exists with same username.",

    recordNotFound: "Record with Id '{0}' not found.",
    invalidLoginCredentials: "Login failed. Invalid Login credentials.",
    loggedInSuccess: "You logged in successfully.",
    maxLoginAttempts: "Too many failed login attempts.",

    currentPasswordIncorrect: "The password you entered is incorrect, please retype your current password.",
    passwordIncorrect: "The password you entered is incorrect.",

    requiredFieldMissing: "Required field is missing! {0}",
    requiredFieldEmail: "The field email is required.",

    emailNotRegistered: "Email not registered!",
    resetPasswordEmailSent: "A link to reset your password has been sent. Please Check your email.",
    emailNotAvailable: "User already exists with same email.",

    messageSentSuccessfully: "Your message was sent successfully.",
    messageNotSent: "Your message was not sent.",
    noMessagesFound: "No messages found.",

    notFound: "No Content Found!",
    badRequest: "Bad request , try again please !"
};