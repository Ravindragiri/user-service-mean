//Once all of the prerequisite software is setup, we can create our Express server:
var express = require('express'),
    //ejsmate = require('ejs-mate'),
    //http = require('http'),
    //path = require('path'),
    //Passport just provides the mechanism to handle authentication leaving the onus of implementing session-handling ourselves and 
    //for that we will be using express- session. 
    //passport = require('passport'),
    //session = require('express-session'),
    bodyParser = require('body-parser');
    //flash = require('connect-flash'),
    //helmet = require('helmet'),
    //config = require('./app/config/config'),
    //mongodb = require('mongodb');
    //mongoUtil = require('./app/common/mongoUtil');
//npm install passport --save
//Adding the --save command will automatically add each dependency to our package.json file. 

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// app.use(session({
//     saveUninitialized: true,
//     resave: true,
//     secret: 'OurSuperSecretCookieSecret',

//     cookieName: 'session',
//     duration: 30 * 60 * 1000,
//     activeDuration: 5 * 60 * 1000
// }));

//app.use(flash());   // use connect-flash for flash messages stored in session
//app.use(passport.initialize());
//app.use(passport.session());
//app.use(helmet())

//require('./app/models/all.server.model');

// Initialize Passport
//var initPassport = require('./app/config/strategies/passport-init');
//initPassport(passport);

//app.set('layout', 'shared/_layout.html');



//app.engine('html', ejsmate);

//var views = [
//    path.join(__dirname, '/app/views')
//];

//app.set('views', views);
//app.set('view engine', 'html');

//allow cross
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:1237');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);

    next();
});

//app.use('/public', express.static(__dirname + '/public'));

require('./app/routes/user.server.routes.js')(app);

app.use(function (err, req, res, next) {
    res.send({
        status: statusCode.error.text,
        message: messages.errorProcessing,
        statusCode: statusCode.error.value
    });
});

app.listen(1337);
console.log("Listening on port 1337");

module.exports = app;